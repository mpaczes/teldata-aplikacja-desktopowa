﻿namespace ToDoList
{
    partial class OrganizatorDnia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kotrolkaKalendarza = new System.Windows.Forms.MonthCalendar();
            this.poleTekstoweStatus = new System.Windows.Forms.TextBox();
            this.kontrolkaWidokListy = new System.Windows.Forms.ListView();
            this.przyciskUsunZaznaczone = new System.Windows.Forms.Button();
            this.etykietaZadaniaDoZrobienia = new System.Windows.Forms.Label();
            this.etykietaUwagiDoZadan = new System.Windows.Forms.Label();
            this.przyciskDodajZadania = new System.Windows.Forms.Button();
            this.poleTekstoweZadaniaDoZrobienia = new System.Windows.Forms.TextBox();
            this.poleTekstoweUwagiDoZadan = new System.Windows.Forms.TextBox();
            this.ramkaDodawanieZadan = new System.Windows.Forms.GroupBox();
            this.przyciskModyfikujZadania = new System.Windows.Forms.Button();
            this.kontrolkaListyRozwijanej = new System.Windows.Forms.ListBox();
            this.etykietaGodzina = new System.Windows.Forms.Label();
            this.ramkaPrzegladanieIUsuwanieZadan = new System.Windows.Forms.GroupBox();
            this.ramkaWyborDnia = new System.Windows.Forms.GroupBox();
            this.etykietaStatus = new System.Windows.Forms.Label();
            this.ramkaDodawanieZadan.SuspendLayout();
            this.ramkaPrzegladanieIUsuwanieZadan.SuspendLayout();
            this.ramkaWyborDnia.SuspendLayout();
            this.SuspendLayout();
            // 
            // kotrolkaKalendarza
            // 
            this.kotrolkaKalendarza.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kotrolkaKalendarza.Location = new System.Drawing.Point(40, 23);
            this.kotrolkaKalendarza.Name = "kotrolkaKalendarza";
            this.kotrolkaKalendarza.ShowWeekNumbers = true;
            this.kotrolkaKalendarza.TabIndex = 1;
            this.kotrolkaKalendarza.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.ZdarzenieZmienionoDateKontrolkaKalendarza);
            this.kotrolkaKalendarza.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.ZdarzenieWybranoDateKontrolkaKalendarza);
            // 
            // poleTekstoweStatus
            // 
            this.poleTekstoweStatus.Location = new System.Drawing.Point(96, 210);
            this.poleTekstoweStatus.Name = "poleTekstoweStatus";
            this.poleTekstoweStatus.ReadOnly = true;
            this.poleTekstoweStatus.Size = new System.Drawing.Size(551, 20);
            this.poleTekstoweStatus.TabIndex = 2;
            // 
            // kontrolkaWidokListy
            // 
            this.kontrolkaWidokListy.AllowColumnReorder = true;
            this.kontrolkaWidokListy.CheckBoxes = true;
            this.kontrolkaWidokListy.FullRowSelect = true;
            this.kontrolkaWidokListy.GridLines = true;
            this.kontrolkaWidokListy.HideSelection = false;
            this.kontrolkaWidokListy.LabelEdit = true;
            this.kontrolkaWidokListy.Location = new System.Drawing.Point(36, 36);
            this.kontrolkaWidokListy.MultiSelect = false;
            this.kontrolkaWidokListy.Name = "kontrolkaWidokListy";
            this.kontrolkaWidokListy.Size = new System.Drawing.Size(771, 220);
            this.kontrolkaWidokListy.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.kontrolkaWidokListy.TabIndex = 3;
            this.kontrolkaWidokListy.UseCompatibleStateImageBehavior = false;
            this.kontrolkaWidokListy.View = System.Windows.Forms.View.Details;
            this.kontrolkaWidokListy.Click += new System.EventHandler(this.ZdarzenieKlikKontrolkaWidokListy);
            // 
            // przyciskUsunZaznaczone
            // 
            this.przyciskUsunZaznaczone.Location = new System.Drawing.Point(847, 39);
            this.przyciskUsunZaznaczone.Name = "przyciskUsunZaznaczone";
            this.przyciskUsunZaznaczone.Size = new System.Drawing.Size(160, 32);
            this.przyciskUsunZaznaczone.TabIndex = 4;
            this.przyciskUsunZaznaczone.Text = "Usuń zaznaczone";
            this.przyciskUsunZaznaczone.UseVisualStyleBackColor = true;
            this.przyciskUsunZaznaczone.Click += new System.EventHandler(this.ZdarzenieKlikUsunZaznaczone);
            // 
            // etykietaZadaniaDoZrobienia
            // 
            this.etykietaZadaniaDoZrobienia.AutoSize = true;
            this.etykietaZadaniaDoZrobienia.Location = new System.Drawing.Point(18, 51);
            this.etykietaZadaniaDoZrobienia.Name = "etykietaZadaniaDoZrobienia";
            this.etykietaZadaniaDoZrobienia.Size = new System.Drawing.Size(112, 13);
            this.etykietaZadaniaDoZrobienia.TabIndex = 5;
            this.etykietaZadaniaDoZrobienia.Text = "Zadania do zrobienia :";
            // 
            // etykietaUwagiDoZadan
            // 
            this.etykietaUwagiDoZadan.AutoSize = true;
            this.etykietaUwagiDoZadan.Location = new System.Drawing.Point(20, 82);
            this.etykietaUwagiDoZadan.Name = "etykietaUwagiDoZadan";
            this.etykietaUwagiDoZadan.Size = new System.Drawing.Size(90, 13);
            this.etykietaUwagiDoZadan.TabIndex = 6;
            this.etykietaUwagiDoZadan.Text = "Uwagi do zadań :";
            // 
            // przyciskDodajZadania
            // 
            this.przyciskDodajZadania.Location = new System.Drawing.Point(58, 117);
            this.przyciskDodajZadania.Name = "przyciskDodajZadania";
            this.przyciskDodajZadania.Size = new System.Drawing.Size(153, 28);
            this.przyciskDodajZadania.TabIndex = 7;
            this.przyciskDodajZadania.Text = "Dodaj zadania";
            this.przyciskDodajZadania.UseVisualStyleBackColor = true;
            this.przyciskDodajZadania.Click += new System.EventHandler(this.ZdarzenieKlikDodajZadania);
            // 
            // poleTekstoweZadaniaDoZrobienia
            // 
            this.poleTekstoweZadaniaDoZrobienia.Location = new System.Drawing.Point(183, 48);
            this.poleTekstoweZadaniaDoZrobienia.Name = "poleTekstoweZadaniaDoZrobienia";
            this.poleTekstoweZadaniaDoZrobienia.Size = new System.Drawing.Size(215, 20);
            this.poleTekstoweZadaniaDoZrobienia.TabIndex = 8;
            // 
            // poleTekstoweUwagiDoZadan
            // 
            this.poleTekstoweUwagiDoZadan.Location = new System.Drawing.Point(183, 79);
            this.poleTekstoweUwagiDoZadan.Name = "poleTekstoweUwagiDoZadan";
            this.poleTekstoweUwagiDoZadan.Size = new System.Drawing.Size(215, 20);
            this.poleTekstoweUwagiDoZadan.TabIndex = 9;
            // 
            // ramkaDodawanieZadan
            // 
            this.ramkaDodawanieZadan.Controls.Add(this.przyciskModyfikujZadania);
            this.ramkaDodawanieZadan.Controls.Add(this.kontrolkaListyRozwijanej);
            this.ramkaDodawanieZadan.Controls.Add(this.etykietaGodzina);
            this.ramkaDodawanieZadan.Controls.Add(this.poleTekstoweUwagiDoZadan);
            this.ramkaDodawanieZadan.Controls.Add(this.poleTekstoweZadaniaDoZrobienia);
            this.ramkaDodawanieZadan.Controls.Add(this.przyciskDodajZadania);
            this.ramkaDodawanieZadan.Controls.Add(this.etykietaUwagiDoZadan);
            this.ramkaDodawanieZadan.Controls.Add(this.etykietaZadaniaDoZrobienia);
            this.ramkaDodawanieZadan.Location = new System.Drawing.Point(705, 12);
            this.ramkaDodawanieZadan.Name = "ramkaDodawanieZadan";
            this.ramkaDodawanieZadan.Size = new System.Drawing.Size(417, 158);
            this.ramkaDodawanieZadan.TabIndex = 10;
            this.ramkaDodawanieZadan.TabStop = false;
            this.ramkaDodawanieZadan.Text = "Dodawanie i modyfikacja zadań";
            // 
            // przyciskModyfikujZadania
            // 
            this.przyciskModyfikujZadania.Location = new System.Drawing.Point(219, 118);
            this.przyciskModyfikujZadania.Name = "przyciskModyfikujZadania";
            this.przyciskModyfikujZadania.Size = new System.Drawing.Size(153, 28);
            this.przyciskModyfikujZadania.TabIndex = 12;
            this.przyciskModyfikujZadania.Text = "Modyfikuj zadania";
            this.przyciskModyfikujZadania.UseVisualStyleBackColor = true;
            this.przyciskModyfikujZadania.Click += new System.EventHandler(this.ZdarzenieKlikModyfikujZadania);
            // 
            // kontrolkaListyRozwijanej
            // 
            this.kontrolkaListyRozwijanej.FormattingEnabled = true;
            this.kontrolkaListyRozwijanej.Location = new System.Drawing.Point(182, 16);
            this.kontrolkaListyRozwijanej.Name = "kontrolkaListyRozwijanej";
            this.kontrolkaListyRozwijanej.Size = new System.Drawing.Size(87, 17);
            this.kontrolkaListyRozwijanej.TabIndex = 11;
            // 
            // etykietaGodzina
            // 
            this.etykietaGodzina.AutoSize = true;
            this.etykietaGodzina.Location = new System.Drawing.Point(18, 21);
            this.etykietaGodzina.Name = "etykietaGodzina";
            this.etykietaGodzina.Size = new System.Drawing.Size(52, 13);
            this.etykietaGodzina.TabIndex = 10;
            this.etykietaGodzina.Text = "Godzina :";
            // 
            // ramkaPrzegladanieIUsuwanieZadan
            // 
            this.ramkaPrzegladanieIUsuwanieZadan.Controls.Add(this.przyciskUsunZaznaczone);
            this.ramkaPrzegladanieIUsuwanieZadan.Controls.Add(this.kontrolkaWidokListy);
            this.ramkaPrzegladanieIUsuwanieZadan.Location = new System.Drawing.Point(41, 277);
            this.ramkaPrzegladanieIUsuwanieZadan.Name = "ramkaPrzegladanieIUsuwanieZadan";
            this.ramkaPrzegladanieIUsuwanieZadan.Size = new System.Drawing.Size(1081, 287);
            this.ramkaPrzegladanieIUsuwanieZadan.TabIndex = 11;
            this.ramkaPrzegladanieIUsuwanieZadan.TabStop = false;
            this.ramkaPrzegladanieIUsuwanieZadan.Text = "Przeglądanie i usuwanie zadań";
            // 
            // ramkaWyborDnia
            // 
            this.ramkaWyborDnia.Controls.Add(this.etykietaStatus);
            this.ramkaWyborDnia.Controls.Add(this.poleTekstoweStatus);
            this.ramkaWyborDnia.Controls.Add(this.kotrolkaKalendarza);
            this.ramkaWyborDnia.Location = new System.Drawing.Point(41, 10);
            this.ramkaWyborDnia.Name = "ramkaWyborDnia";
            this.ramkaWyborDnia.Size = new System.Drawing.Size(653, 251);
            this.ramkaWyborDnia.TabIndex = 12;
            this.ramkaWyborDnia.TabStop = false;
            this.ramkaWyborDnia.Text = "Wybór dnia";
            // 
            // etykietaStatus
            // 
            this.etykietaStatus.AutoSize = true;
            this.etykietaStatus.Location = new System.Drawing.Point(45, 213);
            this.etykietaStatus.Name = "etykietaStatus";
            this.etykietaStatus.Size = new System.Drawing.Size(43, 13);
            this.etykietaStatus.TabIndex = 3;
            this.etykietaStatus.Text = "Status :";
            // 
            // OrganizatorDnia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 650);
            this.Controls.Add(this.ramkaWyborDnia);
            this.Controls.Add(this.ramkaPrzegladanieIUsuwanieZadan);
            this.Controls.Add(this.ramkaDodawanieZadan);
            this.Name = "OrganizatorDnia";
            this.Text = "Organizator dnia";
            this.Load += new System.EventHandler(this.ZdarzenieZaladowanieFormatki);
            this.ramkaDodawanieZadan.ResumeLayout(false);
            this.ramkaDodawanieZadan.PerformLayout();
            this.ramkaPrzegladanieIUsuwanieZadan.ResumeLayout(false);
            this.ramkaWyborDnia.ResumeLayout(false);
            this.ramkaWyborDnia.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.MonthCalendar kotrolkaKalendarza;
        private System.Windows.Forms.TextBox poleTekstoweStatus;
        private System.Windows.Forms.ListView kontrolkaWidokListy;
        private System.Windows.Forms.Button przyciskUsunZaznaczone;
        private System.Windows.Forms.Label etykietaZadaniaDoZrobienia;
        private System.Windows.Forms.Label etykietaUwagiDoZadan;
        private System.Windows.Forms.Button przyciskDodajZadania;
        private System.Windows.Forms.TextBox poleTekstoweZadaniaDoZrobienia;
        private System.Windows.Forms.TextBox poleTekstoweUwagiDoZadan;
        private System.Windows.Forms.GroupBox ramkaDodawanieZadan;
        private System.Windows.Forms.GroupBox ramkaPrzegladanieIUsuwanieZadan;
        private System.Windows.Forms.GroupBox ramkaWyborDnia;
        private System.Windows.Forms.Label etykietaGodzina;
        private System.Windows.Forms.ListBox kontrolkaListyRozwijanej;
        private System.Windows.Forms.Label etykietaStatus;
        private System.Windows.Forms.Button przyciskModyfikujZadania;
    }
}

