﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace ToDoList.Data
{
    class SqLitePodstawoweRepozytorium
    {
        public static string PlikBD
        {
            get { return Environment.CurrentDirectory + "\\ToDoListDb.sqlite"; }
        }

        public static SQLiteConnection ProstePolaczenieBD()
        {
            return new SQLiteConnection("Data Source=" + PlikBD);
        }
    }
}
