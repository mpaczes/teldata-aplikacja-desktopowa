﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Model;
using System.IO;
using Dapper;

namespace ToDoList.Data
{
    class SqLiteDzienMiesiacaRepozytorium : SqLitePodstawoweRepozytorium, IDzienMiesiacaRepozytorium
    {
        public DzienMiesiaca PobierzDzienMiesiaca(int rokMiesiacDzien)
        {
            if (!File.Exists(PlikBD)) return null;

            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();
                DzienMiesiaca result = polaczenie.Query<DzienMiesiaca>(
                    @"SELECT ROK_MIESIAC_DZIEN as RokMiesiacDzien, DZIEN_TYGODNIA as DzienTygodnia, DZIEN_ROKU as DzienRoku, TYDZIEN_ROKU as TydzienRoku
                    FROM DNI_MIESIACA
                    WHERE ROK_MIESIAC_DZIEN = @rokMiesiacDzien", new { rokMiesiacDzien }).FirstOrDefault();
                return result;
            }
        }

        public void ZapiszDzienMiesiaca(DzienMiesiaca dzienMiesiaca)
        {
            if (!File.Exists(PlikBD))
            {
                UtworzBazeDanych();
            }

            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();

                using (var transaction = polaczenie.BeginTransaction())
                {
                    try
                    {
                        int rokMiesiacDzien = dzienMiesiaca.RokMiesiacDzien;

                        DzienMiesiaca result = polaczenie.Query<DzienMiesiaca>(
                            @"SELECT ROK_MIESIAC_DZIEN as RokMiesiacDzien, DZIEN_TYGODNIA as DzienTygodnia, DZIEN_ROKU as DzienRoku, TYDZIEN_ROKU as TydzienRoku
                            FROM DNI_MIESIACA
                            WHERE ROK_MIESIAC_DZIEN = @rokMiesiacDzien", new { rokMiesiacDzien }).FirstOrDefault();

                        if (result == null)
                        {
                            polaczenie.Execute(
                            @"INSERT INTO DNI_MIESIACA 
                            ( ROK_MIESIAC_DZIEN, DZIEN_TYGODNIA, DZIEN_ROKU, TYDZIEN_ROKU ) VALUES 
                            ( @RokMiesiacDzien, @DzienTygodnia, @DzienRoku, @TydzienRoku );
                            select last_insert_rowid()", dzienMiesiaca, transaction);
                        }

                        transaction.Commit();
                    }
                    catch (Exception wyjatek)
                    {
                        transaction.Rollback();
                        throw wyjatek;
                    }
                }
            }
        }

        private static void UtworzBazeDanych()
        {
            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();

                polaczenie.Execute(
                    @"CREATE TABLE DNI_MIESIACA
                      (
                         ROK_MIESIAC_DZIEN      integer not null,
                         DZIEN_TYGODNIA         varchar(30) not null,
                         DZIEN_ROKU             integer,
                         TYDZIEN_ROKU           integer,
                         PRIMARY KEY(ROK_MIESIAC_DZIEN)
                      )");

                polaczenie.Execute(
                    @"CREATE TABLE ZADANIA_NA_DZIEN
                      (
                         ROK_MIESIAC_DZIEN      integer not null,
                         GODZINA                integer not null,
                         ZADANIA_DO_ZROBIENIA   varchar(200) not null,
                         UWAGI                  varchar(200),
                         PRIMARY KEY(ROK_MIESIAC_DZIEN, GODZINA)
                      )");
            }
        }
    }
}
