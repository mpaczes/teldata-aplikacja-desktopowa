﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Model;

namespace ToDoList.Data
{
    interface IDzienMiesiacaRepozytorium
    {
        DzienMiesiaca PobierzDzienMiesiaca(int rokMiesiacDzien);

        void ZapiszDzienMiesiaca(DzienMiesiaca dzienMiesiaca);
    }
}
