﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Model;

namespace ToDoList.Data
{
    interface IZadaniaNaDzienRepozytorium
    {
        List<ZadaniaNaDzien> PobierzZadaniaNaDzien(int rokMiesiacDzien);

        void ZapiszZadaniaNaDzien(ZadaniaNaDzien zadaniaNaDzien);

        int UsunZadaniaNaDzien(int rokMiesiacDzien, List<int> listaGodzin);

        int ZmodyfikujZadania(int rokMiesiacDzien, int godzina, string listaZadan, string uwagi, int oryginalnaGodzina);

        void ZapiszZadaniaNaDzienZLista(DzienMiesiaca dzienMiesiaca, List<ZadaniaNaDzien> listaZadanNaDzien);
    }
}
