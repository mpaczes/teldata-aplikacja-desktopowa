﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Model;
using Dapper;
using System.IO;

namespace ToDoList.Data
{
    class SqLiteZadaniaNaDzienRepozytorium : SqLitePodstawoweRepozytorium, IZadaniaNaDzienRepozytorium
    {
        public List<ZadaniaNaDzien> PobierzZadaniaNaDzien(int rokMiesiacDzien)
        {
            if (!File.Exists(PlikBD)) return null;

            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();
                List<ZadaniaNaDzien> result = polaczenie.Query<ZadaniaNaDzien>(
                    @"SELECT ROK_MIESIAC_DZIEN as RokMiesiacDzien, GODZINA as Godzina, ZADANIA_DO_ZROBIENIA as ZadaniaDoZrobienia, UWAGI as Uwagi
                    FROM ZADANIA_NA_DZIEN
                    WHERE ROK_MIESIAC_DZIEN = @rokMiesiacDzien
                    ORDER BY ROK_MIESIAC_DZIEN, GODZINA", new { rokMiesiacDzien }).ToList<ZadaniaNaDzien>();
                return result;
            }
        }

        public void ZapiszZadaniaNaDzien(ZadaniaNaDzien zadaniaNaDzien)
        {
            if (!File.Exists(PlikBD))
            {
                UtworzBazeDanych();
            }

            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();

                using (var transaction = polaczenie.BeginTransaction())
                {
                    try
                    {
                        polaczenie.Execute(
                        @"INSERT INTO ZADANIA_NA_DZIEN 
                        ( ROK_MIESIAC_DZIEN, GODZINA, ZADANIA_DO_ZROBIENIA, UWAGI ) VALUES 
                        ( @RokMiesiacDzien, @Godzina, @ZadaniaDoZrobienia, @Uwagi );
                        select last_insert_rowid()", zadaniaNaDzien, transaction);

                        transaction.Commit();
                    } catch (Exception wyjatek)
                    {
                        transaction.Rollback();
                        throw wyjatek;
                    }
                }
            }
        }

        public int UsunZadaniaNaDzien(int rokMiesiacDzien, List<int> listaGodzin)
        {
            List<object> listaObiektowDoUsuniecia = new List<object>();

            foreach (int godzina in listaGodzin)
            {
                listaObiektowDoUsuniecia.Add(new { RokMiesiacDzien = rokMiesiacDzien, Godzina = godzina });
            }

            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();

                using (var transaction = polaczenie.BeginTransaction())
                {
                    try
                    {
                        var liczbaUSunietaychWierszy = polaczenie.Execute(
                        @"DELETE FROM ZADANIA_NA_DZIEN WHERE ROK_MIESIAC_DZIEN = @RokMiesiacDzien AND GODZINA = @Godzina",
                        listaObiektowDoUsuniecia.ToArray(),
                        transaction);

                        transaction.Commit();

                        return liczbaUSunietaychWierszy;
                    } catch (Exception wyjatek)
                    {
                        transaction.Rollback();
                        throw wyjatek;
                    }
                }
            }
        }

        public int ZmodyfikujZadania(int rokMiesiacDzien, int godzina, string listaZadan, string uwagi, int oryginalnaGodzina)
        {
            object zadanieDoModyfikacji = new { RokMiesiacDzien = rokMiesiacDzien, Godzina = godzina, ListaZadan = listaZadan, Uwagi = uwagi, OryginalnaGodzina = oryginalnaGodzina };

            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();

                using (var transaction = polaczenie.BeginTransaction())
                {
                    try
                    {
                        var liczbaZmodyfikowanychhWierszy = polaczenie.Execute(
                        @"UPDATE ZADANIA_NA_DZIEN SET GODZINA = @Godzina, ZADANIA_DO_ZROBIENIA = @ListaZadan, UWAGI = @Uwagi WHERE ROK_MIESIAC_DZIEN = @RokMiesiacDzien and GODZINA = @OryginalnaGodzina",
                       zadanieDoModyfikacji,
                       transaction);

                        transaction.Commit();

                        return liczbaZmodyfikowanychhWierszy;
                    }
                    catch (Exception wyjatek)
                    {
                        transaction.Rollback();
                        throw wyjatek;
                    }
                }
            }
        }

        public void ZapiszZadaniaNaDzienZLista(DzienMiesiaca dzienMiesiaca, List<ZadaniaNaDzien> listaZadanNaDzien)
        {
            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();

                using (var transaction = polaczenie.BeginTransaction())
                {
                    try
                    {
                        int rokMiesiacDzien = dzienMiesiaca.RokMiesiacDzien;

                        DzienMiesiaca result = polaczenie.Query<DzienMiesiaca>(
                        @"SELECT ROK_MIESIAC_DZIEN as RokMiesiacDzien, DZIEN_TYGODNIA as DzienTygodnia, DZIEN_ROKU as DzienRoku, TYDZIEN_ROKU as TydzienRoku
                        FROM DNI_MIESIACA
                        WHERE ROK_MIESIAC_DZIEN = @rokMiesiacDzien", new { rokMiesiacDzien }).FirstOrDefault();

                        if (result == null)
                        {
                            var parametrDzienMiesiaca = new DynamicParameters();
                            parametrDzienMiesiaca.Add("@RokMiesiacDzien", dzienMiesiaca.RokMiesiacDzien);
                            parametrDzienMiesiaca.Add("@DzienTygodnia", dzienMiesiaca.DzienTygodnia);
                            parametrDzienMiesiaca.Add("@DzienRoku", dzienMiesiaca.DzienRoku);
                            parametrDzienMiesiaca.Add("@TydzienRoku", dzienMiesiaca.TydzienRoku);

                            var liczbaWstawionychDniMiesiaca = polaczenie.Execute(
                                @"INSERT INTO DNI_MIESIACA
                                (ROK_MIESIAC_DZIEN, DZIEN_TYGODNIA, DZIEN_ROKU, TYDZIEN_ROKU) VALUES
                                (@RokMiesiacDzien, @DzienTygodnia, @DzienRoku, @TydzienRoku)",
                                    parametrDzienMiesiaca,
                                transaction: transaction);
                        }

                        foreach (var zadanieNaDzien in listaZadanNaDzien)
                        {
                            var parametrZadanieNaDzien = new DynamicParameters();
                            parametrZadanieNaDzien.Add("@RokMiesiacDzien", zadanieNaDzien.RokMiesiacDzien);
                            parametrZadanieNaDzien.Add("@Godzina", zadanieNaDzien.Godzina);
                            parametrZadanieNaDzien.Add("@ZadaniaDoZrobienia", zadanieNaDzien.ZadaniaDoZrobienia);
                            parametrZadanieNaDzien.Add("@Uwagi", zadanieNaDzien.Uwagi);

                            var liczbaWstawionycZadanNaGodzine = polaczenie.Execute(
                                @"INSERT INTO ZADANIA_NA_DZIEN 
                                (ROK_MIESIAC_DZIEN, GODZINA, ZADANIA_DO_ZROBIENIA, UWAGI) VALUES 
                                (@RokMiesiacDzien, @Godzina, @ZadaniaDoZrobienia, @Uwagi)",
                                parametrZadanieNaDzien,
                                transaction: transaction);
                        }

                        transaction.Commit();
                    }
                    catch (Exception wyjatek)
                    {
                        transaction.Rollback();
                        throw wyjatek;
                    }
                }
            }
        }

        private static void UtworzBazeDanych()
        {
            using (var polaczenie = ProstePolaczenieBD())
            {
                polaczenie.Open();

                polaczenie.Execute(
                    @"CREATE TABLE DNI_MIESIACA
                      (
                         ROK_MIESIAC_DZIEN      integer not null,
                         DZIEN_TYGODNIA         varchar(30) not null,
                         DZIEN_ROKU             integer,
                         TYDZIEN_ROKU           integer
                      )");

                polaczenie.Execute(
                    @"CREATE TABLE ZADANIA_NA_DZIEN
                      (
                         ROK_MIESIAC_DZIEN      integer not null,
                         GODZINA                integer not null,
                         ZADANIA_DO_ZROBIENIA   varchar(200) not null,
                         UWAGI                  varchar(200)
                      )");
            }
        }
    }
}
