﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ToDoList.Data;
using ToDoList.Model;

namespace ToDoList
{
    public partial class OrganizatorDnia : Form
    {
        private IDzienMiesiacaRepozytorium sqLiteDzienMiesiacaRepozytorium;

        private IZadaniaNaDzienRepozytorium sqLiteZadaniaNaDzienRepozytorium;

        private string oryginalnaGodzina;

        public OrganizatorDnia()
        {
            InitializeComponent();
        }

        private void ZdarzenieKlikUsunZaznaczone(object sender, EventArgs e)
        {
            try
            {
                var zaznaczoneElementy = this.kontrolkaWidokListy.CheckedItems;

                if (zaznaczoneElementy == null || zaznaczoneElementy.Count == 0)
                {
                    poleTekstoweStatus.Text = "Nie wybrano elementów do usunięcia.";
                }
                else
                {
                    List<int> listaGodzinDlaZadan = new List<int>();

                    foreach (ListViewItem zaznaczonyElementIndeks in zaznaczoneElementy)
                    {
                        listaGodzinDlaZadan.Add(int.Parse(zaznaczonyElementIndeks.Text));
                    }

                    DateTime poczatekWybrany = kotrolkaKalendarza.SelectionStart;
                    DateTime koniecWybrany = kotrolkaKalendarza.SelectionEnd;

                    if (poczatekWybrany.ToShortDateString().Equals(koniecWybrany.ToShortDateString()))
                    {
                        int rokMiesiacDzien = int.Parse(Convert.ToString(poczatekWybrany.Year) + Convert.ToString(poczatekWybrany.Month) + Convert.ToString(poczatekWybrany.Day));

                        int liczbaUsunietychWierszy = this.sqLiteZadaniaNaDzienRepozytorium.UsunZadaniaNaDzien(rokMiesiacDzien, listaGodzinDlaZadan);

                        poleTekstoweStatus.Text = "Liczba usuniętych wierszy z bazy danych to : " + liczbaUsunietychWierszy;

                        // odswiezanie listy zadan
                        OdswiezListeZadan();
                    }
                    else
                    {
                        poleTekstoweStatus.Text = "Wybierz tą samą datę.";
                    }
                }
            }
            catch (Exception wyjatek)
            {
                poleTekstoweStatus.Text = "Błąd : " + wyjatek.Message;
            }
        }

        private void ZdarzenieKlikDodajZadania(object sender, EventArgs e)
        {
            try
            {
                DateTime wybranaDataPoczatek = kotrolkaKalendarza.SelectionStart;

                DateTime wybranaDataKoniec = kotrolkaKalendarza.SelectionEnd;

                if (wybranaDataPoczatek.ToShortDateString().Equals(wybranaDataKoniec.ToShortDateString()))
                {
                    int rok = wybranaDataPoczatek.Year;
                    int miesiac = wybranaDataPoczatek.Month;
                    int dzien = wybranaDataPoczatek.Day;
                    string dzienTygodnia = wybranaDataPoczatek.DayOfWeek.ToString();
                    int dzienRoku = wybranaDataPoczatek.DayOfYear;

                    // dzien miesiaca
                    DzienMiesiaca dzienMiesiaca = new DzienMiesiaca();
                    dzienMiesiaca.DzienRoku = dzienRoku;
                    dzienMiesiaca.DzienTygodnia = dzienTygodnia;
                    string rokMiesiacDzienNapis = Convert.ToString(rok) + Convert.ToString(miesiac) + Convert.ToString(dzien);
                    dzienMiesiaca.RokMiesiacDzien = int.Parse(rokMiesiacDzienNapis);

                    // zadania na dzien
                    string zadaniaDoZrobienia = poleTekstoweZadaniaDoZrobienia.Text;
                    string uwagiDoZadan = poleTekstoweUwagiDoZadan.Text;
                    int godzina = int.Parse(kontrolkaListyRozwijanej.Text);

                    ZadaniaNaDzien zadaniaNaDzien = new ZadaniaNaDzien();
                    zadaniaNaDzien.ZadaniaDoZrobienia = zadaniaDoZrobienia;
                    zadaniaNaDzien.Uwagi = uwagiDoZadan;
                    zadaniaNaDzien.Godzina = godzina;
                    zadaniaNaDzien.RokMiesiacDzien = int.Parse(rokMiesiacDzienNapis);

                    List<ZadaniaNaDzien> listaZadan = new List<ZadaniaNaDzien>();
                    listaZadan.Add(zadaniaNaDzien);

                    this.sqLiteZadaniaNaDzienRepozytorium.ZapiszZadaniaNaDzienZLista(dzienMiesiaca, listaZadan);

                    // informacja o sukcesie
                    poleTekstoweStatus.Text = "Dodano do bazy danych beana : " + dzienMiesiaca.RokMiesiacDzien + " z zadaniami.";

                    // czyszczenie pol tekstowych
                    poleTekstoweZadaniaDoZrobienia.Text = "";
                    poleTekstoweUwagiDoZadan.Text = "";

                    // odwiezanie listy zadan
                    OdswiezListeZadan();
                }
            }
            catch (Exception wyjatek)
            {
                poleTekstoweStatus.Text = "Błąd : " + wyjatek.Message;
            }
        }

        private void ZdarzenieWybranoDateKontrolkaKalendarza(object sender, DateRangeEventArgs e)
        {
            try
            {
                this.kontrolkaWidokListy.Clear();

                if (e.Start.ToShortDateString().Equals(e.End.ToShortDateString()))
                {
                    int rokMiesiacDzien = int.Parse(Convert.ToString(e.Start.Year) + Convert.ToString(e.Start.Month) + Convert.ToString(e.Start.Day));

                    List<ZadaniaNaDzien> listaZadan = this.sqLiteZadaniaNaDzienRepozytorium.PobierzZadaniaNaDzien(rokMiesiacDzien);

                    if (listaZadan == null || listaZadan.Capacity == 0)
                    {
                        this.poleTekstoweStatus.Text = "Lista zadań na ten dzień jest pusta.";
                    }
                    else
                    {
                        this.poleTekstoweStatus.Text = "Lista zadań na ten dzień ma " + listaZadan.Capacity + " elementy.";

                        int indeksPetli = 0;
                        List<ListViewItem> listViewItems = new List<ListViewItem>();

                        var kolumny = this.kontrolkaWidokListy.Columns;

                        kolumny.Add("Godzina", 100, HorizontalAlignment.Center);
                        kolumny.Add("Rok / Miesiąc / Dzień", 150, HorizontalAlignment.Center);
                        kolumny.Add("Zadania do zrobienia", 300, HorizontalAlignment.Center);
                        kolumny.Add("Uwagi", 200, HorizontalAlignment.Center);

                        foreach (var zadanieDoZrobienia in listaZadan)
                        {
                            ListViewItem lstViewItem = new ListViewItem(zadanieDoZrobienia.Godzina.ToString(), indeksPetli++);
                            var podElementy = lstViewItem.SubItems;
                            podElementy.Add(Convert.ToString(zadanieDoZrobienia.RokMiesiacDzien));
                            podElementy.Add(zadanieDoZrobienia.ZadaniaDoZrobienia);
                            podElementy.Add(zadanieDoZrobienia.Uwagi);

                            listViewItems.Add(lstViewItem);
                        }

                        this.kontrolkaWidokListy.Items.AddRange(listViewItems.ToArray());
                    }
                }
            }
            catch (Exception wyjatek)
            {
                poleTekstoweStatus.Text = "Błąd : " + wyjatek.Message;
            }
        }

        private void ZdarzenieZmienionoDateKontrolkaKalendarza(object sender, DateRangeEventArgs e)
        {
            this.poleTekstoweStatus.Text = "Zmienoina data : początek =  " +
                e.Start.ToShortDateString() + " : koniec = " + e.End.ToShortDateString();
        }

        private void OdswiezListeZadan()
        {
            try
            {
                this.kontrolkaWidokListy.Clear();

                if (this.kotrolkaKalendarza.SelectionStart.ToShortDateString().Equals(this.kotrolkaKalendarza.SelectionEnd.ToShortDateString()))
                {
                    int rokMiesiacDzien = int.Parse(Convert.ToString(this.kotrolkaKalendarza.SelectionStart.Year) + Convert.ToString(this.kotrolkaKalendarza.SelectionStart.Month) + Convert.ToString(this.kotrolkaKalendarza.SelectionStart.Day));

                    List<ZadaniaNaDzien> listaZadan = this.sqLiteZadaniaNaDzienRepozytorium.PobierzZadaniaNaDzien(rokMiesiacDzien);

                    if (listaZadan == null || listaZadan.Capacity == 0)
                    {
                        this.poleTekstoweStatus.Text = "Lista zadań na ten dzień jest pusta.";
                    }
                    else
                    {
                        this.poleTekstoweStatus.Text = "Lista zadań na ten dzień ma " + listaZadan.Capacity + " elementy.";

                        int indeksPetli = 0;
                        List<ListViewItem> listViewItems = new List<ListViewItem>();

                        var kolumny = this.kontrolkaWidokListy.Columns;

                        kolumny.Add("Godzina", 100, HorizontalAlignment.Center);
                        kolumny.Add("RokMiesiacDzien", 150, HorizontalAlignment.Center);
                        kolumny.Add("Zadania do zrobienia", 300, HorizontalAlignment.Center);
                        kolumny.Add("Uwagi", 200, HorizontalAlignment.Center);

                        foreach (var zadanieDoZrobienia in listaZadan)
                        {
                            ListViewItem lstViewItem = new ListViewItem(zadanieDoZrobienia.Godzina.ToString(), indeksPetli++);
                            var podElementy = lstViewItem.SubItems;
                            podElementy.Add(Convert.ToString(zadanieDoZrobienia.RokMiesiacDzien));
                            podElementy.Add(zadanieDoZrobienia.ZadaniaDoZrobienia);
                            podElementy.Add(zadanieDoZrobienia.Uwagi);

                            listViewItems.Add(lstViewItem);
                        }

                        this.kontrolkaWidokListy.Items.AddRange(listViewItems.ToArray());
                    }
                }
            }
            catch (Exception wyjatek)
            {
                poleTekstoweStatus.Text = "Błąd : " + wyjatek.Message;
            }
        }

        private void ZdarzenieKlikModyfikujZadania(object sender, EventArgs e)
        {
            try
            {
                int rokMiesiacDzien = int.Parse(Convert.ToString(this.kotrolkaKalendarza.SelectionStart.Year) + Convert.ToString(this.kotrolkaKalendarza.SelectionStart.Month) + Convert.ToString(this.kotrolkaKalendarza.SelectionStart.Day));

                int liczbaZmodyfikowanychWierszy = this.sqLiteZadaniaNaDzienRepozytorium.ZmodyfikujZadania(rokMiesiacDzien,
                    int.Parse(this.kontrolkaListyRozwijanej.Text), this.poleTekstoweZadaniaDoZrobienia.Text, this.poleTekstoweUwagiDoZadan.Text, int.Parse(this.oryginalnaGodzina));

                this.poleTekstoweStatus.Text = "Zmodyfikowano wierszy w bazie danych : " + liczbaZmodyfikowanychWierszy;

                // odwiezanie listy zadan
                OdswiezListeZadan();
            }
            catch (Exception wyjatek)
            {
                poleTekstoweStatus.Text = "Błąd : " + wyjatek.Message;
            }
        }

        private void ZdarzenieKlikKontrolkaWidokListy(object sender, EventArgs e)
        {
            // w kontrolce listy rozwijanej moge zaznaczyc tylko jeden wiersz
            if (this.kontrolkaWidokListy.SelectedItems.Count == 1) {
                var wybraneElementy = this.kontrolkaWidokListy.SelectedItems[0];

                this.oryginalnaGodzina = wybraneElementy.SubItems[0].Text;

                string godzina = wybraneElementy.SubItems[0].Text;
                string rokMiesiacDzien = wybraneElementy.SubItems[1].Text;
                string zadaniadoZrobienia = wybraneElementy.SubItems[2].Text;
                string uwagi = wybraneElementy.SubItems[3].Text;

                this.kontrolkaListyRozwijanej.Text = godzina;
                this.poleTekstoweZadaniaDoZrobienia.Text = zadaniadoZrobienia;
                this.poleTekstoweUwagiDoZadan.Text = uwagi;
            } else
            {
                this.poleTekstoweStatus.Text = "Nie ma zdefinoiwanej obsługi zaznaczenia kilku wierszy.";
            }
        }

        private void ZdarzenieZaladowanieFormatki(object sender, EventArgs e)
        {
            // inicjalizacja listy wartosci z godzinami
            this.kontrolkaListyRozwijanej.BeginUpdate();

            for (int x = 1; x <= 23; x++)
            {
                this.kontrolkaListyRozwijanej.Items.Add(x.ToString());
            }

            this.kontrolkaListyRozwijanej.EndUpdate();

            // inicjalizacja repozytoriow
            this.sqLiteDzienMiesiacaRepozytorium = new SqLiteDzienMiesiacaRepozytorium();

            this.sqLiteZadaniaNaDzienRepozytorium = new SqLiteZadaniaNaDzienRepozytorium();

            this.oryginalnaGodzina = "";
        }
    }
}
