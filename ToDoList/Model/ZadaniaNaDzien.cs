﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToDoList.Model
{
    class ZadaniaNaDzien
    {
        public int RokMiesiacDzien { get; set; }

        public int Godzina { get; set; }

        public string ZadaniaDoZrobienia { get; set; }

        public string Uwagi { get; set; }
    }
}
