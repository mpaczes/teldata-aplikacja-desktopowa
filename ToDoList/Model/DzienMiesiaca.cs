﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToDoList.Model
{
    class DzienMiesiaca
    {
        public int RokMiesiacDzien { get; set; }

        public string DzienTygodnia { get; set; }

        public int DzienRoku { get; set; }

        public int TydzienRoku { get; set; }
    }
}
